package com.tchow.kotlinexample2.ui.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import com.tchow.kotlinexample2.R
import com.tchow.kotlinexample2.RedditAPI
import com.tchow.kotlinexample2.data.RedditResponse
import com.tchow.kotlinexample2.ui.adapters.FeedAdapter
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val retrofit = Retrofit.Builder()
                .baseUrl("https://www.reddit.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        val api = retrofit.create(RedditAPI::class.java)

        api.getTop(null, "30").enqueue(object : Callback<RedditResponse> {
            override fun onResponse(call: Call<RedditResponse>?, response: Response<RedditResponse>?) {

                if (response != null) {
                    val children = (response.body() as RedditResponse).data.children

                    recycler_view.layoutManager = LinearLayoutManager(this@MainActivity)
                    recycler_view.adapter = FeedAdapter(children) {

                        val intent = Intent(this@MainActivity, ArticleActivity::class.java)
                        intent.putExtra(ArticleActivity.ARTICLE, it)
                        startActivity(intent)

                    }

                }

            }

            override fun onFailure(call: Call<RedditResponse>?, t: Throwable?) {

            }
        })


    }
}
