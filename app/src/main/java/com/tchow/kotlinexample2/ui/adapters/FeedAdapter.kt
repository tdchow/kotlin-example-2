package com.tchow.kotlinexample2.ui.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.tchow.kotlinexample2.R
import com.tchow.kotlinexample2.data.Child
import kotlinx.android.synthetic.main.article_row.view.*

class FeedAdapter(private val feedList: List<Child>,
                  private val itemClick: (Child) -> Unit) : RecyclerView.Adapter<FeedAdapter.ArticleViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ArticleViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.article_row, parent, false)
        return ArticleViewHolder(view, itemClick)
    }

    override fun onBindViewHolder(holder: ArticleViewHolder?, position: Int) {
        holder?.bindView(feedList[position])
    }

    override fun getItemCount() = feedList.size

    class ArticleViewHolder(view: View,
                            private val itemClick: (Child) -> Unit)
        : RecyclerView.ViewHolder(view) {

        fun bindView(child: Child) {
            with(child.data) {
                Glide.with(itemView).load(thumbnail).into(itemView.article_image_view)
                itemView.article_title_text_view.text = title
                itemView.setOnClickListener { itemClick(child) }
            }
        }
    }
}