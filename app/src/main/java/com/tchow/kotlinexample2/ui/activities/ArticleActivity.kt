package com.tchow.kotlinexample2.ui.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.tchow.kotlinexample2.R
import com.tchow.kotlinexample2.data.Child
import kotlinx.android.synthetic.main.activity_article.*

class ArticleActivity : AppCompatActivity() {

    companion object {
        const val ARTICLE = "ARTICLE"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_article)

        val article = intent.extras.get(ARTICLE)

        if (article is Child) {
            article_title_text_view.text = article.data.title
        }
    }
}