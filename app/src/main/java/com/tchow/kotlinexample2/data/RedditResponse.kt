package com.tchow.kotlinexample2.data

import java.io.Serializable

class RedditResponse(val data: RootData)

class RootData(val children: List<Child>)

class Child(val data: ChildData) : Serializable

class ChildData(val title: String,
                val thumbnail: String) : Serializable