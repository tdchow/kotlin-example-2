package com.tchow.kotlinexample2

import com.tchow.kotlinexample2.data.RedditResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface RedditAPI {
    @GET("/top.json")
    fun getTop(@Query("after") after: String?,
               @Query("limit") limit: String?)
            : Call<RedditResponse>
}